package edu.pjwstk.asd.huffman;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Huffman {
	
	//węzły używane podczas budowania drzewa Huffmana
	private List<Node> nodes;
	//drzewo Huffmana
	Node root;
	
	/**
	 * Na podstawie danych ze strumienia, tworzy listę węzłów(liści)
	 * dla symboli występujących w tekscie i przypisuje im częstości wystąpień.
	 * @param fis
	 */
	public void countFrequencies(FileInputStream fis) {
		nodes = new LinkedList<Node>();
		int c = -1;
		do {
			try {
				c = fis.read();
			} catch (IOException e) {
				System.err.println("Problem z wczytaniem pliku:");
				e.printStackTrace();
			}
			if (c==-1) break;
			Character ch = (char)c;
			System.out.print(ch);
			addOccurrenceToNodes(ch);
		}
		while (true);
		printFrequencies();
	}

	/**
	 * Na częstości wystąpień znaków w pliku wejściowym
	 * tworzy drzwo Huffmana.
	 */
	public void createCodesTree() {
		//TODO
	}
	
	public void printCodes() {
		printCodes(root);
	}
	
	private void printCodes(Node n) {
		//TODO
	}

	private void printFrequencies() {
		System.out.println();
		for (Node n : nodes) {
			System.out.println(n.ch+"("+(int)n.ch+") - "+n.frequency);
		}
	}

	private void addOccurrenceToNodes(Character ch) {
		//TODO
	}

}
